Name:       SDL
Summary:    A cross-platform multimedia library
Version:    1.2.15
Release:    40
License:    LGPLv2+
URL:        http://www.libsdl.org/

Source0:    http://www.libsdl.org/release/%{name}-%{version}.tar.gz
Source1:    SDL_config.h
Patch0:     SDL-1.2.15-add_sdl_config_man.patch
Patch1:     CVE-2019-7637.patch
Patch2:     CVE-2019-7636.patch
Patch3:     CVE-2019-7635_1.patch
Patch4:     CVE-2019-7635_2.patch
Patch5:     CVE-2019-7573_CVE-2019-7576.patch
Patch6:     CVE-2019-7578.patch
Patch7:     CVE-2019-7577.patch
Patch8:     SDL-1.2.10-GrabNotViewable.patch
Patch9:     SDL-1.2.15-const_XData32.patch
Patch10:    SDL-1.2.15-ignore_insane_joystick_axis.patch
Patch11:    SDL-1.2.15-no-default-backing-store.patch
Patch12:    SDL-1.2.15-SDL_EnableUNICODE_drops_keyboard_events.patch
Patch13:    SDL-1.2.15-vec_perm-ppc64le.patch
Patch14:    CVE-2019-13616.patch
Patch15:    backport-CVE-2019-7572-Fix-a-buffer-overread-in-IMA_ADPCM_nibble.patch
Patch16:    backport-CVE-2019-7574-Fix-a-buffer-overread-in-IMA_ADPCM_decode.patch
Patch17:    backport-CVE-2019-7572-Fix-a-buffer-overwrite-in-IMA_ADPCM_decode.patch
Patch18:    backport-CVE-2019-7575-Fix-a-buffer-overwrite-in-MS_ADPCM_decode.patch

BuildRequires:  alsa-lib-devel gdb-headless libtool

%description
Simple DirectMedia Layer(SDL) is a cross-platform development library designed\
to provide low level access to audio, keyboard, mouse, joystick, and graphics\
hardware via OpenGL and Direct3D. It is used by video playback software, emulators,\
and popular games including Valve's award winning catalog and many Humble Bundle games.\

%package devel
Summary:	Development files for %{name}
Provides:	%{name}-static
Obsoletes:	%{name}-static
Requires:	%{name} = %{version}-%{release}
Requires:	alsa-lib-devel mesa-libGL-devel mesa-libGLU-devel
Requires:	libX11-devel libXext-devel libXrandr-devel libXrender-devel

%description devel
Libraries and header files of %{name} are all in the %{name}-devel package.

%package        help
Summary:        Help manual for %{name}

%description    help
The %{name}-help package conatins man manual etc

%prep
%autosetup -n %{name}-%{version} -p1
for F in CREDITS; do
    iconv -f iso8859-1 -t utf-8 < "$F" > "${F}.utf"
    touch --reference "$F" "${F}.utf"
    mv "${F}.utf" "$F"
done
sed -i -e 's/.*AM_PATH_ESD.*//' configure.in
cp -p %{_datadir}/automake-*/config.{sub,guess} build-scripts

%build
aclocal
libtoolize
autoconf
%configure \
    --disable-video-svga \
    --disable-video-ggi \
    --disable-video-aalib \
    --enable-sdl-dlopen \
    --disable-arts \
    --disable-esd \
    --disable-nas \
    --enable-pulseaudio-shared \
    --enable-alsa \
    --disable-video-ps3 \
    --disable-rpath
%make_build

%install
%make_install
# Rename SDL_config.h to SDL_config-<arch>.h to avoid file conflicts on
# multilib systems and install SDL_config.h wrapper
mv %{buildroot}/%{_includedir}/SDL/SDL_config.h %{buildroot}/%{_includedir}/SDL/SDL_config-%{_arch}.h
install -m644 %{SOURCE1} %{buildroot}/%{_includedir}/SDL/SDL_config.h
rm -f %{buildroot}%{_libdir}/*.la

%files
%license COPYING
%doc BUGS CREDITS README-SDL.txt
%{_libdir}/lib*.so.*

%files devel
%doc README docs.html docs/html docs/index.html TODO WhatsNew
%{_bindir}/*-config
%{_libdir}/lib*.so
%{_libdir}/pkgconfig/sdl.pc
%{_includedir}/SDL
%{_datadir}/aclocal/*
%{_libdir}/lib*.a

%files help
%{_mandir}/man1/*
%{_mandir}/man3/SDL*.3*

%changelog
* Fri Nov 18 2022 Wenlong Zhang<zhangwenlong@loongson.cn> - 1.2.15-40
- add loongarch support SDL

* Fri Nov 5 2021 yixiangzhike <yixiangzhike007@163.com> - 1.2.15-39
- DESC: fix CVE-2019-7572 CVE-2019-7574 CVE-2019-7575

* Wed Aug 11 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.2.15-38
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Fri Sep 11 2020 liuweibo <liuweibo10@huawei.com> - 1.2.15-37
- Fix Source0

* Wed Mar 18 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.2.15-36
- Type:CVE
- ID:NA
- SUG:NA
- DESC:Fix CVE-2019-13616

* Thu Jan 16 2020 shijian <shijian16@huawei.com> - 1.2.15-35
- Modify Spec

* Wed Nov 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.2.15-34
- Package init
